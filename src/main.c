#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>
#define HEAP_SIZE 10000

static void test1() {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    void* f_memory = _malloc(100);
    assert(f_memory);
    _free(f_memory);
    heap_term();
}

static void test2() {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);

    void* block1 = _malloc(100);
    void* block2 = _malloc(1000);
    assert(block2 != NULL && block1 != NULL);
    debug_heap(stdout, heap);
    _free(block1);
    _free(block2);
    debug_heap(stdout, heap);
    heap_term();
}
static void test3() {
    void* heap = heap_init(HEAP_SIZE);
    void* block1 = _malloc(100);
    void* block2 = _malloc(1000);
    void* block3 = _malloc(1000);
    assert(block1 != NULL && block2 != NULL && block3 != NULL);
    debug_heap(stdout, heap);

    _free(block1);
    _free(block2);
    debug_heap(stdout, heap);
    heap_term();
}
static void test4() {
    void* heap = heap_init(100);
    assert(heap);
    void* block1 = _malloc(1000);
    assert(block1 != NULL);
    debug_heap(stdout, heap);
    _free(block1);
    debug_heap(stdout, heap);
    heap_term();
}

int main() {
    test1();
    test2();
    test3();
    test4();
    return 0;
}
